<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700&amp;subset=cyrillic" rel="stylesheet">
    <?php wp_head(); ?>
    <title>Website Title</title>
</head>
<body>
    <?php get_header() ?>
    <main id="main" class="site-main" role="main">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
        the_content();
        endwhile; endif ?>
    </main>
    <?php get_footer() ?>
</body>
</html>